# Bravoure - Block Single

## Directive: Code to be added:

        <block-single></block-single>

## Use
it can be added to add a single element of a type


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-block-single": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
