(function () {

    'use strict';

    function blockSingle (PATH_CONFIG, $rootScope) {

        return {
            restrict: "E",
            transclude: false,
            replace: true,

            link: function (scope, element, attrs) {

            },

            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                $rootScope.$emit('Loading', $rootScope.loadedElements);

            },

            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-single/block-single.html'
        }
    }

    blockSingle.$inject = ['PATH_CONFIG', '$rootScope'];

    angular
        .module('bravoureAngularApp')
        .directive('blockSingle', blockSingle);

})();
